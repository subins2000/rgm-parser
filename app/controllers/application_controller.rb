# frozen_string_literal: true

class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def index
  end

  def sample
  end

  def syntactic
  end
end
