# frozen_string_literal: true

module Rsp
  class SubEntryService
    RESULT_SKELETON = {
      suffix: "",
      pronunciation: [],
      type: "",
      english_definition: [],
      malayalam_definition: []
    }

    attr_reader :block
    attr_accessor :result

    def initialize(block)
      @block = block
      @result = RESULT_SKELETON.dup
    end

    def process
      instance_eval(&block)
      result
    end

    private

      def suffix(*words)
        result[:suffix] = words
      end

      def pronunciation(*input)
        result[:pronunciation] = input
      end

      def type(input)
        result[:type] = input
      end

      def english_definition(*definitions)
        result[:english_definition] = definitions
      end

      def malayalam_definition(*definitions)
        result[:malayalam_definition] = definitions
      end
  end
end
