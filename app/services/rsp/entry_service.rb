# frozen_string_literal: true

module Rsp
  class EntryService
    RESULT_SKELETON = {
      root_word: [],
      pronunciation: [],
      type: "",
      english_definition: [],
      malayalam_definition: [],
      sub_entries: []
    }

    attr_reader :block
    attr_accessor :result

    def initialize(block)
      @block = block
      # Deep clone of object
      @result = Marshal.load(Marshal.dump(RESULT_SKELETON))
    end

    def process
      self.instance_eval(&block)
      result
    end

    private

      def root_word(*words)
        result[:root_word] = words
      end

      def pronunciation(*input)
        result[:pronunciation] = input
      end

      def type(input)
        result[:type] = input
      end

      def english_definition(*definitions)
        result[:english_definition] = definitions
      end

      def malayalam_definition(*definitions)
        result[:malayalam_definition] = definitions
      end

      def begin_sub_entry(&sub_entry_block)
        result[:sub_entries].append(SubEntryService.new(sub_entry_block).process)
      end
  end
end
