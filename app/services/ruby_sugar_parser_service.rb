# frozen_string_literal: true

class RubySugarParserService
  attr_reader :input
  attr_accessor :results

  def initialize(input)
    @input = input
    @results = []
  end

  def process
    self.instance_eval(input)

    results
  end

  private

    def begin_entry(&block)
      results.append(Rsp::EntryService.new(block).process)
    end
end
