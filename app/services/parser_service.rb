# frozen_string_literal: true

class ParserService
  STATES = [
    { type: "root_word", matcher: /\*\*(?<result>.*)\*\*/ },
    { type: "pronunciation", matcher: /^(?<result>.*?)\,$/ },
    { type: "root_word_type", matcher: /^(?<result>.*?)\.$/ },
    { type: "english_definition", matcher: /^(?<result>\p{InBasic_Latin}.+)(\:?)$/ },
    { type: "malayalam_definition", matcher: /^(?<result>.*[^\.])/ },
    { type: "sub_words", matcher: /^(?<result>.*[^\.])/ }
  ]
  attr_reader :input
  attr_accessor :current_reading_part, :current_state_index, :current_state_result, :structured_result, :logs

  def initialize(input)
    @input = input
    @current_state_index = 0
    @current_state_result = []
    @structured_result = []
    @logs = []
  end

  def process
    input_parts = input.split(" ")
    current_reading_part_index = 0
    self.current_reading_part = input_parts[current_reading_part_index]

    while current_state.present?
      if matches?
        append_to_current_state_result
        current_reading_part_index += 1
      elsif current_reading_part_is_a_break_character?
        current_reading_part_index += 1
      else
        finish_state and go_to_next_state
      end

      self.current_reading_part = input_parts[current_reading_part_index]
      finish_state and break if current_reading_part.nil?
    end

    [structured_result, logs]
  end

  private

    def log(msg)
      self.logs.append(msg)
    end

    def current_state
      STATES[current_state_index]
    end

    def finish_state
      structured_result.push(
        {
          type: current_state[:type],
          item: current_state_result.join(" ")
        }
      )
    end

    def go_to_next_state
      self.current_state_index = current_state_index + 1
      self.current_state_result = []
    end

    def append_to_current_state_result
      current_state_result.append(current_state_match)
    end

    def current_state_match
      matches = current_reading_part.match(current_state[:matcher])
      matches.present? && matches[:result]
    end

    def matches?
      current_state_match.present?
    end

    def matched_and_added?
      append_to_current_state_result and return true if matches?
    end

    def current_reading_part_is_a_break_character?
      [":", ",", ".", "---"].include?(current_reading_part)
    end
end
