# frozen_string_literal: true

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root to: "application#index"
  post "/", to: "application#index", via: :all
  get "/sample", to: "application#sample"

  get "/syntactic", to: "application#syntactic"
end
